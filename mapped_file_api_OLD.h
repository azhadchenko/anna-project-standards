// mapped_file.h
// VERSION 1.0 RC1
// DATE: 18.03.2016

#ifndef __MAPPED_FILE__
#define __MAPPED_FILE__

#include <fcntl.h>

#ifdef __WINDOWS__
	#include <BaseTsd.h>
	typedef UINT64 uint64_t;
	#define O_RDONLY _O_RDONLY
	#define O_WRONLY _O_WRONLY
	#define O_RDWR   _O_RDWR
#else
	#include <stdint.h>
#endif

typedef enum {
	MF_FILE_READ_ONLY = -10,
	MF_FILE_BUSY,
	MF_NO_MEMORY,
	MF_POOL_FULL,
	MF_ALREADY_RELEASED,
	MF_SYS_OPEN_FAILED,
	MF_INVALID_ARGUMENT,
	MF_INTERNAL_ERROR = -1,
	MF_SUCCESS = 0,
	MF_OUT_OF_RANGE
} mf_error_t;

#ifdef __cplusplus
extern "C"
{
#endif

/*
For all functions listed below:
	Any function (except mf_strerror) might return MF_INTERNAL_ERROR or MF_NO_MEMORY, which may result in undefined behaviour.
	All sizes and offsets are in bytes
	On unload, data must be flushed
	Standards should be backwards-compatible
*/

struct mf_handle_struct;
typedef struct mf_handle_struct mf_handle_t;

/*
	Opens file and initializes mapped file structure, giving a handle to it

	path		- path to the mapped file
 	flag 		- O_RDONLY, O_WRONLY or O_RDWR
	pool_size	- maximum number of entries in the pool
	chunk_size	- chunk size (if 0, it is chosen by the realization)
	mf		- variable to store the handle

	returns	MF_SUCCESS		on success
		MF_SYS_OPEN_FAILED	if opening file failed
		MF_INVALID_ARGUMENT	if pool_size == 0
		MF_INVALID_ARGUMENT	if name or mf == NULL

	On error, content of mf is undefined
*/

mf_error_t mf_open(const char* path, int flag, unsigned pool_size, uint64_t chunk_size,  mf_handle_t* mf);

/*
	Closes the mapped file and frees all the associated data

	mf			- handle to the mapped file

	returns	MF_SUCCESS		on success
		MF_INVALID_ARGUMENT	if mf == NULL
		MF_FILE_BUSY		if some chunks are still acquired
*/
mf_error_t mf_close(mf_handle_t* mf);

/*
	Maps a chunk and gives a pointer to the corresponding memory
	
	mf		- handle to the mapped file
	index		- mapped chunk index
	ptr		- pointer to chunk data memory pointer
	
	returns	MF_SUCCESS		on success
		MF_INVALID_ARGUMENT	if ptr or mf == NULL
		MF_INVALID_ARGUMENT	if index is out of range
		MF_POOL_FULL		if the chunk pool is full
*/
mf_error_t mf_chunk_acquire(mf_handle_t* mf, uint64_t index, void** ptr);

/*
	Establishes that chunk is no longer needed.
	Might unmap a chunk (and might not).
	
	mf		- handle to the mapped file
	index		- mapped chunk index
	
	returns	MF_SUCCESS		on success
		MF_INVALID_ARGUMENT	if mf == NULL
		MF_INVALID_ARGUMENT	if index is out of range
		MF_ALREADY_RELEASED	if chunk is not acquired
*/
mf_error_t mf_chunk_release(mf_handle_t* mf, uint64_t index);

/*
	Flushes a chunk to file
	
	mf			- handle to the mapped file
	index		- mapped chunk index
	
	returns	MF_SUCCESS		on success
		MF_INVALID_ARGUMENT	if mf == NULL
		MF_INVALID_ARGUMENT	if index is out of range
		MF_ALREADY_RELEASED	if chunk is not acquired
*/
mf_error_t mf_chunk_flush(mf_handle_t* mf, uint64_t index);

/*
	Flushes a all mapped data to file

	mf			- handle to the mapped file

	returns	MF_SUCCESS		on success
		MF_INVALID_ARGUMENT	if mf == NULL
*/
mf_error_t mf_file_flush(mf_handle_t* mf);

/*
	Writes chunk size to *size
	
	mf		- handle to the mapped file
	index		- mapped chunk index
	dst		- pointer to store chunk size
	
	returns	MF_SUCCESS		on success
		MF_INVALID_ARGUMENT	if mf == NULL or dst == NULL
		MF_INVALID_ARGUMENT	if index is put of range or chunk is not acquired
*/
mf_error_t mf_get_chunk_size(mf_handle_t* mf, uint64_t index, uint64_t* dst);

/*
	Writes file size to *dst

	mf			- handle to the mapped file
	dst			- pointer to store file size

	returns	MF_SUCCESS		on success
		MF_INVALID_ARGUMENT	if mf == NULL of dst == NULL
*/
mf_error_t mf_get_file_size(mf_handle_t* mf, uint64_t* dst);

/*
	Converts offset to chunk index
	
	mf		- handle to the mapped file
	offset		- the offset in the file
	index		- pointer to store index
	
	returns	MF_SUCCESS		on success
		MF_OUT_OF_RANGE		if offset exceeds the file bounds
		MF_INVALID_ARGUMENT	if mf == NULL of index == NULL

	If function returns MF_OUT_OF_RANGE, index value is undefined
*/
mf_error_t mf_get_chunk_index_from_offset(mf_handle_t* mf, uint64_t offset, uint64_t* index);

/*
	Copies data from mapped file to memory region

	mf		- handle to the mapped file
	size		- size of the copied data
	offset_src	- the offset in the file of the source region
	dst		- pointer to destination data

	returns	MF_SUCCESS		on success
		MF_INVALID_ARGUMENT	if mf == NULL
		MF_INVALID_ARGUMENT	if offset_src region exceeds the file bounds

	If mapped source and destination regions overlap, the behaviour is undefined
*/
mf_error_t mf_read(mf_handle_t* mf, uint64_t size, uint64_t offset_src, void* dst);

/*
	Copies data from memory region to mapped file

	mf		- handle to the mapped file
	size		- size of the copied data
	offset_dst	- the offset in the file of the destination region
	src		- pointer to source data

	returns	MF_SUCCESS		on success
		MF_INVALID_ARGUMENT	if mf == NULL
		MF_INVALID_ARGUMENT	if off_dst region exceeds the file bounds
		MF_FILE_READ_ONLY	if file was opened as read-only

	If source and mapped destination regions overlap, the behaviour is undefined
*/
mf_error_t mf_write(mf_handle_t* mf, uint64_t size, const void* src, uint64_t offset_dst);

/*
	Returns a string that describes the error code passed in the argument errcode

	errcode		- the error code, returned by some function from this library

	returns	        static string: description of the error
*/
char* mf_strerror(mf_error_t errcode);

#ifdef __cplusplus
}
#endif

#endif // __MAPPED_FILE__
