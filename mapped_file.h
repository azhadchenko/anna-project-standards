// mapped_file.h
// VERSION 1.0 RC2
// DATE: 20.03.2016

#ifndef __MAPPED_FILE__
#define __MAPPED_FILE__

#if _WIN32 || _WIN64
	#include <BaseTsd.h>
	typedef UINT64 uint64_t;
#else
	#include <stdint.h>
#endif

#define MF_SUCCESS			0
#define MF_OUT_OF_RANGE		1

#define MF_INTERNAL_ERROR	-1
#define MF_OPEN_FAILED		-2
#define MF_INVALID_VALUE	-3
#define MF_NO_MEMORY		-4
#define MF_FILE_BUSY		-5
#define MF_POOL_FULL		-6
#define MF_ALREADY_RELEASED	-7
#define MF_READ_ONLY		-8

#ifdef __cplusplus
extern "C"
{
#endif

/*
	Returns a string that describes the error code passed in the argument errcode

	errcode		- the error code, returned by some function from this library

	returns	description of the error
*/
const char* mf_strerror(mf_error_t errcode);

/*
For all functions listed below:
	MF_INTERNAL_ERROR, MF_FILE_BUSY and MF_NO_MEMORY may result in undefine behaviour in the future
	Any function might return MF_INTERNAL_ERROR or MF_NO_MEMORY
	All functions might flush any chunk
	All sizes and offsets are in bytes
	On unload, chunks must be flushed
	Standards should be backwards-compatible
*/

typedef void* mf_handle_t;

/*
	Opens file and initializes mapped file structure, giving a handle to it
	
	name		- mapped file name
	pool_size	- maximum number of entries in the pool
	chunk_size	- chunk size (if 0, it is chosen by the realiztion)
	read_only	- if not 0, nothing is written to the file (even if mapped memory contents are changed)
	mf			- variable to store the handle
	
	returns	MF_SUCCESS			on success
			MF_OPEN_FAILED		if opening file failed
			MF_INVALID_VALUE	if pool_size == 0
			MF_INVALID_VALUE	if name or mf == NULL
			
	On error, contents of mf are undefined
*/
int mf_open(const char* name, unsigned pool_size, uint64_t chunk_size, int read_only, mf_handle_t* mf);

/*
	Closes the mapped file and frees all the associated data
	
	mf			- handle to the mapped file
	
	returns	MF_SUCCESS			on success
			MF_INVALID_VALUE	if mf == NULL
			MF_FILE_BUSY		if some chunks have non-null reference counters
*/
int mf_close(mf_handle_t mf);

/*
	Maps a chunk, increments reference counter and gives a pointer to the corresponding memory
	Might unmap a chunk with 0 reference counter to reuse it (and must, if no free chunks are left)
	
	mf			- handle to the mapped file
	index			- mapped chunk index
	ptr			- pointer to chunk data memory pointer
	
	returns	MF_SUCCESS			on success
			MF_INVALID_VALUE	if ptr or mf == NULL
			MF_INVALID_VALUE	if index is out of range
			MF_POOL_FULL		if the chunk pool is full (and threre are no chunks with 0 references)
*/
int mf_chunk_acquire(mf_handle_t mf, uint64_t index, void** ptr);

/*
	Decrements reference counter for the given chunk
	Might unmap a chunk with 0 reference counter
	Chunk might stay mapped if its reference count is 0
	
	mf			- handle to the mapped file
	index		- mapped chunk index
	flush		- if not 0 and reference counter reaches 0 during this call, chunk is flushed to the file
	
	returns	MF_SUCCESS			on success
			MF_INVALID_VALUE	if mf == NULL
			MF_INVALID_VALUE	if index is out of range
			MF_ALREADY_RELEASED	if reference count is already 0
*/
int mf_chunk_release(mf_handle_t mf, uint64_t index, int flush);

/*
	Flushes a chunk to file
	
	mf			- handle to the mapped file
	index		- mapped chunk index
	
	returns	MF_SUCCESS			on success
			MF_INVALID_VALUE	if mf == NULL
			MF_INVALID_VALUE	if index is out of range
			MF_ALREADY_RELEASED	if reference count is already 0
*/
int mf_chunk_flush(mf_handle_t mf, uint64_t index);

/*
	Flushes a all mapped data to file

	mf			- handle to the mapped file

	returns	MF_SUCCESS		on success
			MF_INVALID_ARGUMENT	if mf == NULL
*/
mf_error_t mf_file_flush(mf_handle_t* mf);

/*
	Writes chunk size to *size
	
	mf			- handle to the mapped file
	index		- chunk index
	dst			- pointer to store chunk size
	
	returns	MF_SUCCESS			on success
			MF_INVALID_VALUE	if mf == NULL or dst == NULL
			MF_INVALID_VALUE	if index is out of range
*/
int mf_chunk_size(mf_handle_t mf, uint64_t index, uint64_t* dst);

/*
	Writes file size to *size
	
	mf			- handle to the mapped file
	dst			- pointer to store file size
	
	returns	MF_SUCCESS			on success
			MF_INVALID_VALUE	if mf == NULL of dest == NULL
*/
int mf_file_size(mf_handle_t mf, uint64_t* dst);

/*
	Converts offset to chunk index
	
	mf			- handle to the mapped file
	offset		- the offset in the file
	index		- pointer to store index
	
	returns	MF_SUCCESS			on success
			MF_OUT_OF_RANGE		if offset exceeds the file bounds
			MF_INVALID_VALUE	if mf == NULL of index == NULL

	If function returns MF_OUT_OF_RANGE, index value is undefined
*/
int mf_index_from_offset(mf_handle_t mf, uint64_t offset, uint64_t* index);

/*
	Copies data from mapped file to memory region
	
	mf			- handle to the mapped file
	dst			- pointer to destination data
	offset_src	- the offset in the file of the source region
	size		- size of the copied data
	
	returns	MF_SUCCESS			on success
			MF_INVALID_VALUE	if mf == NULL
			MF_INVALID_VALUE	if offset_src region exceeds the file bounds
			
	If mapped source and destination regions overlap, the behaviour is undefined
*/
int mf_read(mf_handle_t mf, void* dst, uint64_t offset_src, uint64_t size);

/*
	Copies data from memory region to mapped file
	
	mf			- handle to the mapped file
	offset_dst	- the offset in the file of the destination region
	src			- pointer to source data
	size		- size of the copied data
	flush		- flush data to disk for modified mapped chunks
	
	returns	MF_SUCCESS			on success
			MF_INVALID_VALUE	if mf == NULL
			MF_INVALID_VALUE	if off_dst region exceeds the file bounds
			MF_READ_ONLY		if file was opened as read-only
			
	If source and mapped destination regions overlap, the behaviour is undefined
*/
int mf_write(mf_handle_t mf, uint64_t offset_dst, const void* src, uint64_t size, int flush);

#ifdef __cplusplus
}
#endif

#endif // __MAPPED_FILE__
